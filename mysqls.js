/* 配置数据库 */
const mysql = require("mysql")

const mysqlOption = {
  pool: null,

  option: {
    host: "localhost",
    port: 3306,
    user: "root",
    password: "123456",
    database: "mymysql",
    connectionLimit: 100, //连接数，
    wait_timeout: 5000, //最大连接超时时间
    interactive_timeout: 10000, //交互式超时时间
  },

  create: function () {
    //创建连接池
    if (!this.pool) {
      this.pool = mysql.createPool(this.option)
    }
  },

  exec: function (data) {
    // 1.创建连接
    this.create()

    // 2.进行数据库操作
    this.pool.getConnection((err, conn) => {
      if (err) return console.log("mysql pool err" + err)

      //3.数据库查询操作
      conn.query(data.sql, data.params, (err, res) => {
        // 把数据回调回去
        data.success && data.success(res)
        data.error && data.error(err)
      })

      //4.释放连接池
      conn.release()
    })
  },
}

module.exports = mysqlOption
