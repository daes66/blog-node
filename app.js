/*  职责

  -- 管理模块 --
  -- 启动服务器 --
  -- node app.js -- 

*/

const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const router = require('./router') //导入路由

const app = new express()

// 开放路径
app.use('/public', express.static(path.join(__dirname,'./public')))
app.use('/node_modules',express.static(path.join(__dirname,'./node_modules')))


// 解决跨域问题
app.all("/*", function(req, res, next) {
  // 跨域处理
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
  res.header("X-Powered-By", ' 3.2.1');
  res.header("Content-Type", "application/json;charset=utf-8");
  next(); // 执行下一个路由
})

//post请求需要use的   敲中间  这里设置上传大文件
app.use(bodyParser.json({limit:'50mb'}));
app.use(bodyParser.urlencoded({limit:'50mb',extended:true}));

// 将public文件夹托管为静态文件
app.use('/public', express.static(__dirname + '/public/upload/'))

// 挂载路由
app.use(router)

app.listen(3000, () => console.log('server running...'))
