/*
职责 
 token验证
*/

const jwt = require('jsonwebtoken')

const secret = 'aaabbbfadwesoow15eiiaf'

// 创建token
function createToken(payload){
    payload.timer = new Date()
    payload.exp = 60 * 60 * 2 * 1000
    return jwt.sign(payload, secret)
}

// 使用token
function checkToken(token){
    return new Promise((resolve, reject) => {
        jwt.verify(token, secret, (err, res) => {
            if(!err) return resolve(res)
            reject('token验证失败')
        })
    })
}

// 导出
module.exports = {
    createToken, checkToken
}
