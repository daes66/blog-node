/* 职责

     -- 管理数据库 --
     -- 配置数据库 --

*/

var mysql = require('mysql')

// 创建数据库 配置
const handleMysql = {
  // 配置路由
  config:{
    host: 'localhost',
    post: 3306,
    user: 'root',
    password: '123456',
    database: 'blog',  //库名称
    // connectionLimit: 1000 //连接数，
    wait_timeout: 10000,  //最大连接超时时间
    interactive_timeout: 10000  //交互式超时时间
  },
  pool: null,
  /* 
    创建连接池
     */
  create: function() {
    const that = this
    // 没有pool 就创建连接池
    if (!that.pool) {
      that.pool = mysql.createPool(that.config)
    }
  },

  // 创建连接池
  exec: function(data) {
    const that = this
    // 创建
    that.create()
    that.pool.getConnection((err, conn) => {
      if (err) return console.log('mysql pool err' + err)
      
      conn.query(data.sql, data.params, (err, res) => {
        // 成功返回把值返回res
        if (data.success) {
          data.success(res)
        }
        // 失败返回失败参数给err
        if (data.err) {
          data.err(err)
        }
      })
      conn.release() //释放连接池
    })
  }
    

} 


// 导出封装好的数据库连接池
module.exports = handleMysql

